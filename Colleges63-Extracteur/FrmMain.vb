﻿Imports System.IO
Imports OfficeOpenXml
Imports OfficeOpenXml.Table
Imports Colleges63_Extracteur.Extensions

Public Class FrmMain

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub BtnSourceFile_Click(sender As Object, e As EventArgs) Handles BtnSourceFile.Click
        Using ofd As New OpenFileDialog
            With ofd
                .Title = "Choisissez votre fichier excel (.xlsx) :"
                .Multiselect = False
                .CheckFileExists = True
                .Filter = "Excel(*.xlsx)|*.xlsx"
                .FileName = ""
                If .ShowDialog() = DialogResult.OK Then
                    TxbSourceFile.Text = .FileName
                    Try
                        TxbInventoryIWSColNum.Text = ""
                        TxbInventoryIWSNumberPeda.Text = ""
                        TxbInventoryIWSNumberAdmin.Text = ""
                        TxbInventoryIWSNumber.Text = ""
                        TxbInventorySE3ColNum.Text = ""
                        TxbInventorySE3Number.Text = ""
                        TsTxbExistsInIws.Text = ""
                        TsTxbNotExistsInIws.Text = ""

                        LvSe3.Items.Clear()
                        LvIws.Items.Clear()
                        TsTxbExistsInSe3.Text = ""
                        TsTxbNotExistsInSe3.Text = ""

                        Dim newFile As FileInfo = New FileInfo(TxbSourceFile.Text)

                        If newFile.Length = 0 Then
                            MessageBox.Show("Ce fichier est vide !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Exit Sub
                        Else
                            Dim Se3MachineNames As New List(Of Machine)
                            Dim iwsMachineNames As New List(Of Machine)

                            Using pck As ExcelPackage = New ExcelPackage(newFile)
                                Using wkbook As ExcelWorkbook = pck.Workbook
                                    Dim ExcelMergeQueryExists = wkbook.Worksheets.Any(Function(f) f.Name.ToLower = "excelmergequery")

                                    If ExcelMergeQueryExists = False Then
                                        MessageBox.Show("Ce fichier ne contient pas le feuillet nommé ""excelmergequery""." & vbNewLine & "S'agit-il vraiment d'une extracton IWS !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        Exit Sub
                                    Else
                                        Dim wkSheets As ExcelWorksheet = wkbook.Worksheets(1)
                                        Dim colCount As Integer = wkSheets.Dimension.End.Column
                                        Dim rowCount As Integer = wkSheets.Dimension.End.Row
                                        Dim CodeBarreColExists As Boolean
                                        Dim CodeBarreColIndex As Integer
                                        Dim FonctionColExists As Boolean
                                        Dim FonctionColIndex As Integer
                                        Dim Se3ColExists As Boolean
                                        Dim Se3ColIndex As Integer

                                        For iCol As Integer = 1 To colCount
                                            Using Rng As ExcelRange = wkSheets.Cells(1, iCol)
                                                Dim tblcollection As ExcelTableCollection = wkSheets.Tables
                                                Dim table As ExcelTable = tblcollection.Add(Rng, "Tbl" & iCol.ToString)
                                                table.ShowFilter = False

                                                Dim val = Rng.Value
                                                If Not val Is Nothing AndAlso val.ToString().ToLower.Trim() = "code barre" Then
                                                    CodeBarreColExists = True
                                                    CodeBarreColIndex = iCol
                                                    TxbInventoryIWSColNum.Text = iCol.ToString
                                                ElseIf Not val Is Nothing AndAlso val.ToString().ToLower.Trim() = "fonction" Then
                                                    FonctionColExists = True
                                                    FonctionColIndex = iCol
                                                End If
                                            End Using
                                        Next

                                        If CodeBarreColExists = False Then
                                            MessageBox.Show("Ce fichier ne contient pas la colonne nommée ""Code barre"" dans le tableau IWS !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                            Exit Sub
                                        End If

                                        If FonctionColExists = False Then
                                            MessageBox.Show("Ce fichier ne contient pas la colonne nommée ""Fonction"" dans le tableau IWS !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                            Exit Sub
                                        End If


                                        If CodeBarreColExists AndAlso FonctionColExists Then
                                            For iRow As Integer = 2 To rowCount
                                                Dim val = wkSheets.Cells(iRow, CodeBarreColIndex).Value
                                                Dim valFonction = wkSheets.Cells(iRow, FonctionColIndex).Value

                                                If Not val Is Nothing AndAlso Not valFonction Is Nothing Then
                                                    iwsMachineNames.Add(New Machine With {.OriginalName = val.ToString().Trim, .Fonction = valFonction.ToString().ToUpper.Trim()})
                                                End If
                                            Next

                                            If iwsMachineNames.Count = 0 Then
                                                MessageBox.Show("Ce fichier ne contient pas d'éléments dans la colonne ""Code barre"" du tableau IWS !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                                Exit Sub
                                            Else
                                                Dim PedaNumber = iwsMachineNames.Where(Function(f) f.Fonction.ToUpper = "PEDAGOGIQUE")
                                                Dim AdminNumber = iwsMachineNames.Where(Function(f) f.Fonction.ToUpper = "ADMINISTRATIF")

                                                TxbInventoryIWSNumberPeda.Text = PedaNumber.Count.ToString
                                                TxbInventoryIWSNumberAdmin.Text = AdminNumber.Count.ToString
                                                TxbInventoryIWSNumber.Text = (PedaNumber.Count + AdminNumber.Count).ToString

                                                For iCol As Integer = 1 To colCount
                                                    Using Rng As ExcelRange = wkSheets.Cells(2, iCol)
                                                        Dim val = Rng.Value
                                                        If Not val Is Nothing AndAlso Not Rng.Hyperlink Is Nothing Then
                                                            Se3ColExists = True
                                                            Se3ColIndex = iCol
                                                            TxbInventorySE3ColNum.Text = iCol.ToString
                                                            Exit For
                                                        End If
                                                    End Using
                                                Next

                                                If Se3ColExists = False Then
                                                    MessageBox.Show("Ce fichier ne contient pas d'éléments conformes dans le tableau SE3 !!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                                    Exit Sub
                                                Else

                                                    For iRow As Integer = 2 To rowCount
                                                        Using Rng As ExcelRange = wkSheets.Cells(iRow, Se3ColIndex)
                                                            Dim val = Rng.Value
                                                            If Not val Is Nothing AndAlso Not Rng.Hyperlink Is Nothing Then
                                                                Se3MachineNames.Add(New Machine With {.OriginalName = val.ToString().Trim})
                                                            End If
                                                        End Using
                                                    Next

                                                    TxbInventorySE3Number.Text = Se3MachineNames.Count.ToString

                                                    For Each machine In Se3MachineNames
                                                        Dim str = New String(4) {}
                                                        str(0) = machine.OriginalName
                                                        Dim Exists = iwsMachineNames.Any(Function(f) f.OriginalName.Contains(machine.OriginalName.ToUpper))

                                                        If Exists = False Then
                                                            If machine.OriginalName.ToUpper.StartsWith("PC") Then
                                                                Dim machineNew = machine.OriginalName.Substring(2)
                                                                Exists = iwsMachineNames.Any(Function(f) f.OriginalName.Contains(machineNew.ToUpper))
                                                                If Exists Then
                                                                    machine.OriginalName = machineNew
                                                                    For Each mach In iwsMachineNames
                                                                        If mach.OriginalName.Contains(machine.OriginalName.ToUpper) Then
                                                                            machine.Fonction = mach.Fonction
                                                                            Exit For
                                                                        End If
                                                                    Next
                                                                End If
                                                            End If
                                                        Else
                                                            For Each mach In iwsMachineNames
                                                                If mach.OriginalName.Contains(machine.OriginalName.ToUpper) Then
                                                                    machine.OriginalName = mach.OriginalName
                                                                    machine.Fonction = mach.Fonction
                                                                    Exit For
                                                                End If
                                                            Next
                                                        End If

                                                        str(1) = If(Exists, "OUI", "NON")
                                                        str(2) = If(Exists, machine.OriginalName, "")
                                                        str(3) = machine.Fonction
                                                        Dim lvi As New ListViewItem(str)
                                                        If Exists Then
                                                            lvi.BackColor = Color.LightGreen
                                                        Else
                                                            lvi.BackColor = Color.LightGray
                                                        End If
                                                        LvSe3.Items.Add(lvi)
                                                    Next

                                                    Dim Corresponds As Integer = LvSe3.Items.Cast(Of ListViewItem).Where(Function(f) f.BackColor = Color.LightGreen).Count
                                                    TsTxbExistsInIws.Text = Corresponds.ToString
                                                    Dim NotInIWS As Integer = LvSe3.Items.Cast(Of ListViewItem).Where(Function(f) f.BackColor = Color.LightGray).Count
                                                    TsTxbNotExistsInIws.Text = NotInIWS.ToString

                                                    For Each machine In iwsMachineNames
                                                        Dim str = New String(4) {}
                                                        str(0) = machine.OriginalName
                                                        str(1) = "NON"
                                                        str(3) = machine.Fonction
                                                        Dim lvi As New ListViewItem(str)
                                                        lvi.BackColor = Color.LightGray
                                                        LvIws.Items.Add(lvi)

                                                        For Each it As ListViewItem In LvSe3.Items
                                                            If it.SubItems(2).Text = machine.OriginalName AndAlso it.SubItems(1).Text = "OUI" Then
                                                                Dim currentLvi As ListViewItem = LvIws.Items.Cast(Of ListViewItem).Where(Function(f) f.Text = it.SubItems(2).Text).First
                                                                currentLvi.SubItems(2).Text = it.Text
                                                                currentLvi.SubItems(1).Text = "OUI"
                                                                currentLvi.BackColor = Color.LightGreen
                                                            End If
                                                        Next
                                                    Next

                                                    Dim Corresponds2 As Integer = LvIws.Items.Cast(Of ListViewItem).Where(Function(f) f.BackColor = Color.LightGreen).Count
                                                    TsTxbExistsInSe3.Text = Corresponds2.ToString
                                                    Dim NotInIWS2 As Integer = LvIws.Items.Cast(Of ListViewItem).Where(Function(f) f.BackColor = Color.LightGray).Count
                                                    TsTxbNotExistsInSe3.Text = NotInIWS2.ToString

                                                    GbxInventoryIWS.Enabled = True
                                                    GbxInventorySE3.Enabled = True
                                                    GbxResult.Enabled = True
                                                    GbxExport.Enabled = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End Using
                            End Using
                        End If
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        GbxInventoryIWS.Enabled = False
                        GbxInventorySE3.Enabled = False
                        GbxResult.Enabled = False
                        GbxExport.Enabled = False
                    End Try
                End If
            End With
        End Using
    End Sub

    Private Sub BtnExport_Click(sender As Object, e As EventArgs) Handles BtnExport.Click
        Using sfd As New SaveFileDialog
            With sfd
                .Title = "Saisissez l'emplacement d'enregistrement :"
                .Filter = "Excel(*.xlsx)|*.xlsx"
                .DefaultExt = ".xlsx"
                .FileName = Date.Now.ToString("ddMMyyyy") & "_" & New FileInfo(TxbSourceFile.Text).Name
                If .ShowDialog() = DialogResult.OK Then
                    Try
                        Using pck As ExcelPackage = New ExcelPackage()
                            Dim wkbook As ExcelWorkbook = pck.Workbook
                            Dim WorkSheetSE3 = wkbook.Worksheets.Add("SE3")

                            FillTableau(WorkSheetSE3, False)

                            Dim WorkSheetIWS = wkbook.Worksheets.Add("IWS")

                            FillTableau(WorkSheetIWS, True)

                            pck.SaveAs(New FileInfo(.FileName))

                            If File.Exists(.FileName) Then
                                MessageBox.Show("Fichier Excel enregistré avec succès.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End Using
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If
            End With
        End Using
    End Sub

    Private Sub FillTableau(WorkSheetSE3 As ExcelWorksheet, isIWS As Boolean)
        Dim iRow As Integer = 1

        Dim LISTV As ListViewEx = If(isIWS, LvIws, LvSe3)
        For Each machineSE3 As ListViewItem In LISTV.Items
            For iCol As Integer = 0 To 3
                If iRow = 1 Then
                    Dim Rng As ExcelRange
                    If iCol + 1 = 1 Then
                        Rng = Utils.SetRange(WorkSheetSE3.Cells(1, 1), True, Style.ExcelBorderStyle.Medium, False, Color.LightBlue)
                        Rng.Value = "Nom machine " & If(isIWS, "IWS", "SE3")
                    ElseIf iCol + 1 = 2 Then
                        Rng = Utils.SetRange(WorkSheetSE3.Cells(1, 2), True, Style.ExcelBorderStyle.Medium, False, Color.LightBlue)
                        Rng.Value = "Existe dans " & If(isIWS, "IWS", "SE3")
                    ElseIf iCol + 1 = 3 Then
                        Rng = Utils.SetRange(WorkSheetSE3.Cells(1, 3), True, Style.ExcelBorderStyle.Medium, False, Color.LightBlue)
                        Rng.Value = "Nom machine " & If(isIWS, "SE3", "IWS")
                    ElseIf iCol + 1 = 4 Then
                        Rng = Utils.SetRange(WorkSheetSE3.Cells(1, 4), True, Style.ExcelBorderStyle.Medium, False, Color.LightBlue)
                        Rng.Value = "Fonction IWS"
                    End If
                Else
                    Dim Rng As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(iRow, iCol + 1), True, Style.ExcelBorderStyle.Thin, False, Color.LightGray)
                    Rng.Value = machineSE3.SubItems(iCol).Text
                    Rng.AutoFitColumns()
                End If
            Next
            If machineSE3.SubItems(1).Text = "OUI" Then
                Dim Rng As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(iRow, 1, iRow, 4), True, Style.ExcelBorderStyle.Thin, False, Color.LightGreen)
                Rng.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
            End If
            iRow += 1
        Next

        If isIWS = False Then
            Dim RngTotalTitle As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(4, 6), True, Style.ExcelBorderStyle.Medium, False)
            RngTotalTitle.Value = "Nombre total de machines"
            RngTotalTitle.AutoFitColumns()

            Dim RngTotalCount As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(4, 7), True, Style.ExcelBorderStyle.Thin, False)
            RngTotalCount.Value = TxbInventorySE3Number.Text

            Dim RngInIwsTitle As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(6, 6), True, Style.ExcelBorderStyle.Medium, False)
            RngInIwsTitle.Value = "Existe dans IWS"
            Dim RngInIws As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(6, 7), True, Style.ExcelBorderStyle.Thin, False)
            RngInIws.Value = TsTxbExistsInIws.Text
            '					
            Dim RngNotInIwsTitle As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(8, 6), True, Style.ExcelBorderStyle.Medium, False)
            RngNotInIwsTitle.Value = "N'existe pas dans IWS"
            Dim RngNotInIws As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(8, 7), True, Style.ExcelBorderStyle.Thin, False)
            RngNotInIws.Value = TsTxbNotExistsInIws.Text
        Else
            Dim RngTotalTitle As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(4, 6), True, Style.ExcelBorderStyle.Medium, False)
            RngTotalTitle.Value = "Nombre total de machines"
            RngTotalTitle.AutoFitColumns()

            Dim RngTotalCount As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(4, 7), True, Style.ExcelBorderStyle.Thin, False)
            RngTotalCount.Value = TxbInventoryIWSNumber.Text

            Dim RngInIwsTitle As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(6, 6), True, Style.ExcelBorderStyle.Medium, False)
            RngInIwsTitle.Value = "Existe dans SE3"
            Dim RngInIws As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(6, 7), True, Style.ExcelBorderStyle.Thin, False)
            RngInIws.Value = TsTxbExistsInSe3.Text
            '					
            Dim RngNotInIwsTitle As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(8, 6), True, Style.ExcelBorderStyle.Medium, False)
            RngNotInIwsTitle.Value = "N'existe pas dans SE3"
            Dim RngNotInIws As ExcelRange = Utils.SetRange(WorkSheetSE3.Cells(8, 7), True, Style.ExcelBorderStyle.Thin, False)
            RngNotInIws.Value = TsTxbNotExistsInSe3.Text

        End If

    End Sub

End Class

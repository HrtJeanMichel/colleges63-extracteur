﻿Imports System.ComponentModel
Imports Colleges63_Extracteur.Win32

Namespace Extensions

    Public Class ListViewEx
        Inherits ListView

#Region " Fields "
        Private m_elv As Boolean = False
        '''	<summary>
        '''	Required designer variable.
        '''	</summary>
        Private components As Container = Nothing
#End Region

#Region " Constructor "
        Public Sub New()
            InitializeComponent()
            MyBase.FullRowSelect = True
            MyBase.AllowColumnReorder = True
        End Sub
#End Region

#Region " Methods "
        '''	<summary>
        '''	Clean up any resources being used.
        '''	</summary>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        '''	<summary>
        '''	Required method	for	Designer support - do not modify 
        '''	the	contents of	this method	with the code editor.
        '''	</summary>
        Private Sub InitializeComponent()
            components = New Container()
        End Sub

        Private Sub SetWindowTheme()
            NativeMethods.SetWindowTheme(Me.Handle, "explorer", Nothing)
            NativeMethods.SendMessage(Me.Handle, NativeConstants.LVM_SETEXTENDEDLISTVIEWSTYLE, NativeConstants.LVS_EX_DOUBLEBUFFER, NativeConstants.LVS_EX_DOUBLEBUFFER)
        End Sub

        '<SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
        Protected Overrides Sub WndProc(ByRef msg As Message)
            Select Case msg.Msg
                Case NativeConstants.WM_PAINT
                    If Not m_elv Then
                        SetWindowTheme()
                        m_elv = True
                    End If
                    Exit Select
            End Select
            MyBase.WndProc(msg)
        End Sub
        #End Region
        
    End Class

End Namespace

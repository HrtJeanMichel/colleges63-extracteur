﻿Imports System.Runtime.InteropServices
Imports System.Text

Namespace Win32
    Public NotInheritable Class NativeMethods

#Region " Extensions.ListViewEx "
        <DllImport("user32.dll")>
        Public Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wPar As Integer, lPar As Integer) As IntPtr
        End Function

        <DllImport("uxtheme.dll", CharSet:=CharSet.Unicode)>
        Public Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
        End Function
#End Region

    End Class
End Namespace

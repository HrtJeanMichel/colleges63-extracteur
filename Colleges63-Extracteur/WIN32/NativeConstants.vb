﻿Namespace Win32

    Public NotInheritable Class NativeConstants

#Region " Extensions.ListViewEx "
        Public Const LVM_FIRST As Integer = &H1000
        Public Const LVM_SETEXTENDEDLISTVIEWSTYLE As Integer = LVM_FIRST + 54
        Public Const LVS_EX_DOUBLEBUFFER As Integer = 65536
        Public Const WM_PAINT As Integer = 15
#End Region

    End Class
End Namespace

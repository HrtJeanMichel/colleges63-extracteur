﻿Imports Colleges63_Extracteur.Extensions

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.GbxSourceFile = New System.Windows.Forms.GroupBox()
        Me.TxbSourceFile = New System.Windows.Forms.TextBox()
        Me.LblSourceFile = New System.Windows.Forms.Label()
        Me.BtnSourceFile = New System.Windows.Forms.Button()
        Me.GbxInventoryIWS = New System.Windows.Forms.GroupBox()
        Me.pictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TxbInventoryIWSColNum = New System.Windows.Forms.TextBox()
        Me.LblInventoryIWSColNum = New System.Windows.Forms.Label()
        Me.TxbInventoryIWSNumber = New System.Windows.Forms.TextBox()
        Me.LblInventoryIWSNumberTotal = New System.Windows.Forms.Label()
        Me.TxbInventoryIWSNumberAdmin = New System.Windows.Forms.TextBox()
        Me.TxbInventoryIWSNumberPeda = New System.Windows.Forms.TextBox()
        Me.LblInventoryIWSNumberAdmin = New System.Windows.Forms.Label()
        Me.LblInventoryIWSNumberPeda = New System.Windows.Forms.Label()
        Me.GbxInventorySE3 = New System.Windows.Forms.GroupBox()
        Me.pictureBox2 = New System.Windows.Forms.PictureBox()
        Me.TxbInventorySE3ColNum = New System.Windows.Forms.TextBox()
        Me.LblInventorySE3ColNum = New System.Windows.Forms.Label()
        Me.TxbInventorySE3Number = New System.Windows.Forms.TextBox()
        Me.LblInventoryIWSNumber = New System.Windows.Forms.Label()
        Me.GbxResult = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TpIws = New System.Windows.Forms.TabPage()
        Me.LvIws = New ListViewEx()
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader9 = New System.Windows.Forms.ColumnHeader()
        Me.TsIws = New System.Windows.Forms.ToolStrip()
        Me.TsTxbExistsInSe3 = New System.Windows.Forms.ToolStripTextBox()
        Me.TsLblExistsInSe3 = New System.Windows.Forms.ToolStripLabel()
        Me.TsTxbNotExistsInSe3 = New System.Windows.Forms.ToolStripTextBox()
        Me.TsLblNotExistsInSe3 = New System.Windows.Forms.ToolStripLabel()
        Me.TpSe3 = New System.Windows.Forms.TabPage()
        Me.TsSe3 = New System.Windows.Forms.ToolStrip()
        Me.TsTxbExistsInIws = New System.Windows.Forms.ToolStripTextBox()
        Me.TsLblExistsInIws = New System.Windows.Forms.ToolStripLabel()
        Me.TsTxbNotExistsInIws = New System.Windows.Forms.ToolStripTextBox()
        Me.TsLblNotExistsInIws = New System.Windows.Forms.ToolStripLabel()
        Me.LvSe3 = New ListViewEx()
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader()
        Me.imageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GbxExport = New System.Windows.Forms.GroupBox()
        Me.BtnExport = New System.Windows.Forms.Button()
        Me.GbxSourceFile.SuspendLayout()
        Me.GbxInventoryIWS.SuspendLayout()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxInventorySE3.SuspendLayout()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxResult.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TpIws.SuspendLayout()
        Me.TsIws.SuspendLayout()
        Me.TpSe3.SuspendLayout()
        Me.TsSe3.SuspendLayout()
        Me.GbxExport.SuspendLayout()
        Me.SuspendLayout()
        '
        'GbxSourceFile
        '
        Me.GbxSourceFile.Controls.Add(Me.TxbSourceFile)
        Me.GbxSourceFile.Controls.Add(Me.LblSourceFile)
        Me.GbxSourceFile.Controls.Add(Me.BtnSourceFile)
        Me.GbxSourceFile.Location = New System.Drawing.Point(12, 12)
        Me.GbxSourceFile.Name = "GbxSourceFile"
        Me.GbxSourceFile.Size = New System.Drawing.Size(847, 61)
        Me.GbxSourceFile.TabIndex = 0
        Me.GbxSourceFile.TabStop = False
        Me.GbxSourceFile.Text = "Sélection du fichier source Excel"
        '
        'TxbSourceFile
        '
        Me.TxbSourceFile.BackColor = System.Drawing.Color.White
        Me.TxbSourceFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbSourceFile.Location = New System.Drawing.Point(85, 24)
        Me.TxbSourceFile.Name = "TxbSourceFile"
        Me.TxbSourceFile.ReadOnly = True
        Me.TxbSourceFile.Size = New System.Drawing.Size(663, 20)
        Me.TxbSourceFile.TabIndex = 2
        '
        'LblSourceFile
        '
        Me.LblSourceFile.AutoSize = True
        Me.LblSourceFile.Location = New System.Drawing.Point(31, 27)
        Me.LblSourceFile.Name = "LblSourceFile"
        Me.LblSourceFile.Size = New System.Drawing.Size(48, 13)
        Me.LblSourceFile.TabIndex = 1
        Me.LblSourceFile.Text = "Chemin :"
        '
        'BtnSourceFile
        '
        Me.BtnSourceFile.Location = New System.Drawing.Point(754, 22)
        Me.BtnSourceFile.Name = "BtnSourceFile"
        Me.BtnSourceFile.Size = New System.Drawing.Size(75, 23)
        Me.BtnSourceFile.TabIndex = 0
        Me.BtnSourceFile.Text = "Parcourir"
        Me.BtnSourceFile.UseVisualStyleBackColor = True
        '
        'GbxInventoryIWS
        '
        Me.GbxInventoryIWS.Controls.Add(Me.pictureBox1)
        Me.GbxInventoryIWS.Controls.Add(Me.TxbInventoryIWSColNum)
        Me.GbxInventoryIWS.Controls.Add(Me.LblInventoryIWSColNum)
        Me.GbxInventoryIWS.Controls.Add(Me.TxbInventoryIWSNumber)
        Me.GbxInventoryIWS.Controls.Add(Me.LblInventoryIWSNumberTotal)
        Me.GbxInventoryIWS.Controls.Add(Me.TxbInventoryIWSNumberAdmin)
        Me.GbxInventoryIWS.Controls.Add(Me.TxbInventoryIWSNumberPeda)
        Me.GbxInventoryIWS.Controls.Add(Me.LblInventoryIWSNumberAdmin)
        Me.GbxInventoryIWS.Controls.Add(Me.LblInventoryIWSNumberPeda)
        Me.GbxInventoryIWS.Enabled = False
        Me.GbxInventoryIWS.Location = New System.Drawing.Point(12, 79)
        Me.GbxInventoryIWS.Name = "GbxInventoryIWS"
        Me.GbxInventoryIWS.Size = New System.Drawing.Size(847, 61)
        Me.GbxInventoryIWS.TabIndex = 3
        Me.GbxInventoryIWS.TabStop = False
        Me.GbxInventoryIWS.Text = "Nombre de PC dans l'extraction IWS"
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(40, 21)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(31, 30)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pictureBox1.TabIndex = 11
        Me.pictureBox1.TabStop = False
        '
        'TxbInventoryIWSColNum
        '
        Me.TxbInventoryIWSColNum.BackColor = System.Drawing.Color.White
        Me.TxbInventoryIWSColNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbInventoryIWSColNum.Location = New System.Drawing.Point(157, 25)
        Me.TxbInventoryIWSColNum.Name = "TxbInventoryIWSColNum"
        Me.TxbInventoryIWSColNum.ReadOnly = True
        Me.TxbInventoryIWSColNum.Size = New System.Drawing.Size(29, 20)
        Me.TxbInventoryIWSColNum.TabIndex = 8
        Me.TxbInventoryIWSColNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblInventoryIWSColNum
        '
        Me.LblInventoryIWSColNum.AutoSize = True
        Me.LblInventoryIWSColNum.Location = New System.Drawing.Point(85, 28)
        Me.LblInventoryIWSColNum.Name = "LblInventoryIWSColNum"
        Me.LblInventoryIWSColNum.Size = New System.Drawing.Size(66, 13)
        Me.LblInventoryIWSColNum.TabIndex = 7
        Me.LblInventoryIWSColNum.Text = "N° colonne :"
        '
        'TxbInventoryIWSNumber
        '
        Me.TxbInventoryIWSNumber.BackColor = System.Drawing.Color.White
        Me.TxbInventoryIWSNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbInventoryIWSNumber.Location = New System.Drawing.Point(754, 25)
        Me.TxbInventoryIWSNumber.Name = "TxbInventoryIWSNumber"
        Me.TxbInventoryIWSNumber.ReadOnly = True
        Me.TxbInventoryIWSNumber.Size = New System.Drawing.Size(75, 20)
        Me.TxbInventoryIWSNumber.TabIndex = 6
        Me.TxbInventoryIWSNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblInventoryIWSNumberTotal
        '
        Me.LblInventoryIWSNumberTotal.AutoSize = True
        Me.LblInventoryIWSNumberTotal.Location = New System.Drawing.Point(700, 28)
        Me.LblInventoryIWSNumberTotal.Name = "LblInventoryIWSNumberTotal"
        Me.LblInventoryIWSNumberTotal.Size = New System.Drawing.Size(48, 13)
        Me.LblInventoryIWSNumberTotal.TabIndex = 5
        Me.LblInventoryIWSNumberTotal.Text = "TOTAL :"
        '
        'TxbInventoryIWSNumberAdmin
        '
        Me.TxbInventoryIWSNumberAdmin.BackColor = System.Drawing.Color.White
        Me.TxbInventoryIWSNumberAdmin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbInventoryIWSNumberAdmin.Location = New System.Drawing.Point(545, 25)
        Me.TxbInventoryIWSNumberAdmin.Name = "TxbInventoryIWSNumberAdmin"
        Me.TxbInventoryIWSNumberAdmin.ReadOnly = True
        Me.TxbInventoryIWSNumberAdmin.Size = New System.Drawing.Size(75, 20)
        Me.TxbInventoryIWSNumberAdmin.TabIndex = 4
        Me.TxbInventoryIWSNumberAdmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxbInventoryIWSNumberPeda
        '
        Me.TxbInventoryIWSNumberPeda.BackColor = System.Drawing.Color.White
        Me.TxbInventoryIWSNumberPeda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbInventoryIWSNumberPeda.Location = New System.Drawing.Point(389, 25)
        Me.TxbInventoryIWSNumberPeda.Name = "TxbInventoryIWSNumberPeda"
        Me.TxbInventoryIWSNumberPeda.ReadOnly = True
        Me.TxbInventoryIWSNumberPeda.Size = New System.Drawing.Size(75, 20)
        Me.TxbInventoryIWSNumberPeda.TabIndex = 3
        Me.TxbInventoryIWSNumberPeda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblInventoryIWSNumberAdmin
        '
        Me.LblInventoryIWSNumberAdmin.AutoSize = True
        Me.LblInventoryIWSNumberAdmin.Location = New System.Drawing.Point(470, 28)
        Me.LblInventoryIWSNumberAdmin.Name = "LblInventoryIWSNumberAdmin"
        Me.LblInventoryIWSNumberAdmin.Size = New System.Drawing.Size(69, 13)
        Me.LblInventoryIWSNumberAdmin.TabIndex = 3
        Me.LblInventoryIWSNumberAdmin.Text = "Administratif :"
        '
        'LblInventoryIWSNumberPeda
        '
        Me.LblInventoryIWSNumberPeda.AutoSize = True
        Me.LblInventoryIWSNumberPeda.Location = New System.Drawing.Point(307, 28)
        Me.LblInventoryIWSNumberPeda.Name = "LblInventoryIWSNumberPeda"
        Me.LblInventoryIWSNumberPeda.Size = New System.Drawing.Size(76, 13)
        Me.LblInventoryIWSNumberPeda.TabIndex = 2
        Me.LblInventoryIWSNumberPeda.Text = "Pédagogique :"
        '
        'GbxInventorySE3
        '
        Me.GbxInventorySE3.Controls.Add(Me.pictureBox2)
        Me.GbxInventorySE3.Controls.Add(Me.TxbInventorySE3ColNum)
        Me.GbxInventorySE3.Controls.Add(Me.LblInventorySE3ColNum)
        Me.GbxInventorySE3.Controls.Add(Me.TxbInventorySE3Number)
        Me.GbxInventorySE3.Controls.Add(Me.LblInventoryIWSNumber)
        Me.GbxInventorySE3.Enabled = False
        Me.GbxInventorySE3.Location = New System.Drawing.Point(12, 146)
        Me.GbxInventorySE3.Name = "GbxInventorySE3"
        Me.GbxInventorySE3.Size = New System.Drawing.Size(847, 61)
        Me.GbxInventorySE3.TabIndex = 4
        Me.GbxInventorySE3.TabStop = False
        Me.GbxInventorySE3.Text = "Nombre de PC dans l'extraction SE3"
        '
        'pictureBox2
        '
        Me.pictureBox2.Image = CType(resources.GetObject("pictureBox2.Image"), System.Drawing.Image)
        Me.pictureBox2.Location = New System.Drawing.Point(40, 21)
        Me.pictureBox2.Name = "pictureBox2"
        Me.pictureBox2.Size = New System.Drawing.Size(31, 30)
        Me.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pictureBox2.TabIndex = 12
        Me.pictureBox2.TabStop = False
        '
        'TxbInventorySE3ColNum
        '
        Me.TxbInventorySE3ColNum.BackColor = System.Drawing.Color.White
        Me.TxbInventorySE3ColNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbInventorySE3ColNum.Location = New System.Drawing.Point(157, 25)
        Me.TxbInventorySE3ColNum.Name = "TxbInventorySE3ColNum"
        Me.TxbInventorySE3ColNum.ReadOnly = True
        Me.TxbInventorySE3ColNum.Size = New System.Drawing.Size(29, 20)
        Me.TxbInventorySE3ColNum.TabIndex = 10
        Me.TxbInventorySE3ColNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblInventorySE3ColNum
        '
        Me.LblInventorySE3ColNum.AutoSize = True
        Me.LblInventorySE3ColNum.Location = New System.Drawing.Point(85, 28)
        Me.LblInventorySE3ColNum.Name = "LblInventorySE3ColNum"
        Me.LblInventorySE3ColNum.Size = New System.Drawing.Size(66, 13)
        Me.LblInventorySE3ColNum.TabIndex = 9
        Me.LblInventorySE3ColNum.Text = "N° colonne :"
        '
        'TxbInventorySE3Number
        '
        Me.TxbInventorySE3Number.BackColor = System.Drawing.Color.White
        Me.TxbInventorySE3Number.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxbInventorySE3Number.Location = New System.Drawing.Point(754, 25)
        Me.TxbInventorySE3Number.Name = "TxbInventorySE3Number"
        Me.TxbInventorySE3Number.ReadOnly = True
        Me.TxbInventorySE3Number.Size = New System.Drawing.Size(75, 20)
        Me.TxbInventorySE3Number.TabIndex = 8
        Me.TxbInventorySE3Number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblInventoryIWSNumber
        '
        Me.LblInventoryIWSNumber.AutoSize = True
        Me.LblInventoryIWSNumber.Location = New System.Drawing.Point(700, 28)
        Me.LblInventoryIWSNumber.Name = "LblInventoryIWSNumber"
        Me.LblInventoryIWSNumber.Size = New System.Drawing.Size(48, 13)
        Me.LblInventoryIWSNumber.TabIndex = 7
        Me.LblInventoryIWSNumber.Text = "TOTAL :"
        '
        'GbxResult
        '
        Me.GbxResult.Controls.Add(Me.TabControl1)
        Me.GbxResult.Enabled = False
        Me.GbxResult.Location = New System.Drawing.Point(12, 213)
        Me.GbxResult.Name = "GbxResult"
        Me.GbxResult.Size = New System.Drawing.Size(847, 448)
        Me.GbxResult.TabIndex = 5
        Me.GbxResult.TabStop = False
        Me.GbxResult.Text = "Résultat"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TpIws)
        Me.TabControl1.Controls.Add(Me.TpSe3)
        Me.TabControl1.ImageList = Me.imageList1
        Me.TabControl1.Location = New System.Drawing.Point(6, 19)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(835, 423)
        Me.TabControl1.TabIndex = 0
        '
        'TpIws
        '
        Me.TpIws.Controls.Add(Me.LvIws)
        Me.TpIws.Controls.Add(Me.TsIws)
        Me.TpIws.ImageIndex = 1
        Me.TpIws.Location = New System.Drawing.Point(4, 30)
        Me.TpIws.Name = "TpIws"
        Me.TpIws.Padding = New System.Windows.Forms.Padding(3)
        Me.TpIws.Size = New System.Drawing.Size(827, 389)
        Me.TpIws.TabIndex = 1
        Me.TpIws.Text = "Source = IWS"
        Me.TpIws.UseVisualStyleBackColor = True
        '
        'LvIws
        '
        Me.LvIws.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader9})
        Me.LvIws.FullRowSelect = True
        Me.LvIws.Location = New System.Drawing.Point(0, 31)
        Me.LvIws.Name = "LvIws"
        Me.LvIws.Size = New System.Drawing.Size(827, 366)
        Me.LvIws.TabIndex = 5
        Me.LvIws.UseCompatibleStateImageBehavior = False
        Me.LvIws.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Nom machine IWS"
        Me.ColumnHeader5.Width = 234
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Existe dans SE3"
        Me.ColumnHeader6.Width = 90
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Nom machine SE3"
        Me.ColumnHeader7.Width = 369
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Fonction IWS"
        Me.ColumnHeader9.Width = 108
        '
        'TsIws
        '
        Me.TsIws.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsIws.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsTxbExistsInSe3, Me.TsLblExistsInSe3, Me.TsTxbNotExistsInSe3, Me.TsLblNotExistsInSe3})
        Me.TsIws.Location = New System.Drawing.Point(3, 3)
        Me.TsIws.Name = "TsIws"
        Me.TsIws.Size = New System.Drawing.Size(821, 25)
        Me.TsIws.TabIndex = 4
        Me.TsIws.Text = "ToolStrip2"
        '
        'TsTxbExistsInSe3
        '
        Me.TsTxbExistsInSe3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsTxbExistsInSe3.BackColor = System.Drawing.Color.White
        Me.TsTxbExistsInSe3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TsTxbExistsInSe3.Name = "TsTxbExistsInSe3"
        Me.TsTxbExistsInSe3.ReadOnly = True
        Me.TsTxbExistsInSe3.Size = New System.Drawing.Size(80, 25)
        Me.TsTxbExistsInSe3.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TsLblExistsInSe3
        '
        Me.TsLblExistsInSe3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsLblExistsInSe3.Name = "TsLblExistsInSe3"
        Me.TsLblExistsInSe3.Size = New System.Drawing.Size(91, 22)
        Me.TsLblExistsInSe3.Text = "Existe dans SE3 :"
        '
        'TsTxbNotExistsInSe3
        '
        Me.TsTxbNotExistsInSe3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsTxbNotExistsInSe3.BackColor = System.Drawing.Color.White
        Me.TsTxbNotExistsInSe3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TsTxbNotExistsInSe3.Name = "TsTxbNotExistsInSe3"
        Me.TsTxbNotExistsInSe3.ReadOnly = True
        Me.TsTxbNotExistsInSe3.Size = New System.Drawing.Size(80, 25)
        Me.TsTxbNotExistsInSe3.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TsLblNotExistsInSe3
        '
        Me.TsLblNotExistsInSe3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsLblNotExistsInSe3.Name = "TsLblNotExistsInSe3"
        Me.TsLblNotExistsInSe3.Size = New System.Drawing.Size(124, 22)
        Me.TsLblNotExistsInSe3.Text = "N'existe pas dans SE3 :"
        '
        'TpSe3
        '
        Me.TpSe3.Controls.Add(Me.TsSe3)
        Me.TpSe3.Controls.Add(Me.LvSe3)
        Me.TpSe3.ImageIndex = 0
        Me.TpSe3.Location = New System.Drawing.Point(4, 30)
        Me.TpSe3.Name = "TpSe3"
        Me.TpSe3.Padding = New System.Windows.Forms.Padding(3)
        Me.TpSe3.Size = New System.Drawing.Size(827, 389)
        Me.TpSe3.TabIndex = 0
        Me.TpSe3.Text = "Source = SE3"
        Me.TpSe3.UseVisualStyleBackColor = True
        '
        'TsSe3
        '
        Me.TsSe3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsSe3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsTxbExistsInIws, Me.TsLblExistsInIws, Me.TsTxbNotExistsInIws, Me.TsLblNotExistsInIws})
        Me.TsSe3.Location = New System.Drawing.Point(3, 3)
        Me.TsSe3.Name = "TsSe3"
        Me.TsSe3.Size = New System.Drawing.Size(821, 25)
        Me.TsSe3.TabIndex = 3
        Me.TsSe3.Text = "ToolStrip1"
        '
        'TsTxbExistsInIws
        '
        Me.TsTxbExistsInIws.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsTxbExistsInIws.BackColor = System.Drawing.Color.White
        Me.TsTxbExistsInIws.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TsTxbExistsInIws.Name = "TsTxbExistsInIws"
        Me.TsTxbExistsInIws.ReadOnly = True
        Me.TsTxbExistsInIws.Size = New System.Drawing.Size(80, 25)
        Me.TsTxbExistsInIws.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TsLblExistsInIws
        '
        Me.TsLblExistsInIws.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsLblExistsInIws.Name = "TsLblExistsInIws"
        Me.TsLblExistsInIws.Size = New System.Drawing.Size(93, 22)
        Me.TsLblExistsInIws.Text = "Existe dans IWS :"
        '
        'TsTxbNotExistsInIws
        '
        Me.TsTxbNotExistsInIws.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsTxbNotExistsInIws.BackColor = System.Drawing.Color.White
        Me.TsTxbNotExistsInIws.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TsTxbNotExistsInIws.Name = "TsTxbNotExistsInIws"
        Me.TsTxbNotExistsInIws.ReadOnly = True
        Me.TsTxbNotExistsInIws.Size = New System.Drawing.Size(80, 25)
        Me.TsTxbNotExistsInIws.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TsLblNotExistsInIws
        '
        Me.TsLblNotExistsInIws.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsLblNotExistsInIws.Name = "TsLblNotExistsInIws"
        Me.TsLblNotExistsInIws.Size = New System.Drawing.Size(126, 22)
        Me.TsLblNotExistsInIws.Text = "N'existe pas dans IWS :"
        '
        'LvSe3
        '
        Me.LvSe3.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.LvSe3.FullRowSelect = True
        Me.LvSe3.Location = New System.Drawing.Point(0, 31)
        Me.LvSe3.Name = "LvSe3"
        Me.LvSe3.Size = New System.Drawing.Size(827, 366)
        Me.LvSe3.TabIndex = 2
        Me.LvSe3.UseCompatibleStateImageBehavior = False
        Me.LvSe3.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Nom machine SE3"
        Me.ColumnHeader1.Width = 230
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Existe dans IWS"
        Me.ColumnHeader2.Width = 90
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Nom machine IWS"
        Me.ColumnHeader3.Width = 350
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Fonction IWS"
        Me.ColumnHeader4.Width = 100
        '
        'imageList1
        '
        Me.imageList1.ImageStream = CType(resources.GetObject("imageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.imageList1.Images.SetKeyName(0, "se3.PNG")
        Me.imageList1.Images.SetKeyName(1, "isilog.jpg")
        '
        'GbxExport
        '
        Me.GbxExport.Controls.Add(Me.BtnExport)
        Me.GbxExport.Enabled = False
        Me.GbxExport.Location = New System.Drawing.Point(12, 667)
        Me.GbxExport.Name = "GbxExport"
        Me.GbxExport.Size = New System.Drawing.Size(847, 61)
        Me.GbxExport.TabIndex = 6
        Me.GbxExport.TabStop = False
        Me.GbxExport.Text = "Export dans un fichier Excel"
        '
        'BtnExport
        '
        Me.BtnExport.Location = New System.Drawing.Point(10, 19)
        Me.BtnExport.Name = "BtnExport"
        Me.BtnExport.Size = New System.Drawing.Size(824, 36)
        Me.BtnExport.TabIndex = 1
        Me.BtnExport.Text = "Exporter"
        Me.BtnExport.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(871, 736)
        Me.Controls.Add(Me.GbxExport)
        Me.Controls.Add(Me.GbxResult)
        Me.Controls.Add(Me.GbxInventorySE3)
        Me.Controls.Add(Me.GbxInventoryIWS)
        Me.Controls.Add(Me.GbxSourceFile)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Colleges63-Extracteur"
        Me.GbxSourceFile.ResumeLayout(False)
        Me.GbxSourceFile.PerformLayout()
        Me.GbxInventoryIWS.ResumeLayout(False)
        Me.GbxInventoryIWS.PerformLayout()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxInventorySE3.ResumeLayout(False)
        Me.GbxInventorySE3.PerformLayout()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxResult.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TpIws.ResumeLayout(False)
        Me.TpIws.PerformLayout()
        Me.TsIws.ResumeLayout(False)
        Me.TsIws.PerformLayout()
        Me.TpSe3.ResumeLayout(False)
        Me.TpSe3.PerformLayout()
        Me.TsSe3.ResumeLayout(False)
        Me.TsSe3.PerformLayout()
        Me.GbxExport.ResumeLayout(False)
        Me.ResumeLayout(False)
    End Sub
    Private pictureBox2 As System.Windows.Forms.PictureBox
    Private pictureBox1 As System.Windows.Forms.PictureBox
    Private imageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GbxExport As System.Windows.Forms.GroupBox
    Friend WithEvents BtnExport As System.Windows.Forms.Button
    Friend WithEvents GbxSourceFile As GroupBox
    Friend WithEvents BtnSourceFile As Button
    Friend WithEvents TxbSourceFile As TextBox
    Friend WithEvents LblSourceFile As Label
    Friend WithEvents GbxInventoryIWS As GroupBox
    Friend WithEvents GbxInventorySE3 As GroupBox
    Friend WithEvents TxbInventoryIWSNumberAdmin As TextBox
    Friend WithEvents TxbInventoryIWSNumberPeda As TextBox
    Friend WithEvents LblInventoryIWSNumberAdmin As Label
    Friend WithEvents LblInventoryIWSNumberPeda As Label
    Friend WithEvents TxbInventoryIWSNumber As TextBox
    Friend WithEvents LblInventoryIWSNumberTotal As Label
    Friend WithEvents TxbInventorySE3Number As TextBox
    Friend WithEvents LblInventoryIWSNumber As Label
    Friend WithEvents GbxResult As GroupBox
    Friend WithEvents TxbInventoryIWSColNum As TextBox
    Friend WithEvents LblInventoryIWSColNum As Label
    Friend WithEvents TxbInventorySE3ColNum As TextBox
    Friend WithEvents LblInventorySE3ColNum As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TpSe3 As TabPage
    Friend WithEvents TsSe3 As ToolStrip
    Friend WithEvents TsTxbExistsInIws As ToolStripTextBox
    Friend WithEvents TsLblNotExistsInIws As ToolStripLabel
    Friend WithEvents LvSe3 As ListViewEx
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents TpIws As TabPage
    Friend WithEvents LvIws As ListViewEx
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents TsIws As ToolStrip
    Friend WithEvents TsTxbExistsInSe3 As ToolStripTextBox
    Friend WithEvents TsLblExistsInSe3 As ToolStripLabel
    Friend WithEvents TsLblExistsInIws As ToolStripLabel
    Friend WithEvents TsTxbNotExistsInIws As ToolStripTextBox
    Friend WithEvents TsTxbNotExistsInSe3 As ToolStripTextBox
    Friend WithEvents TsLblNotExistsInSe3 As ToolStripLabel
End Class

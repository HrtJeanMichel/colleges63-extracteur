﻿
Imports OfficeOpenXml

Public Class Utils
	
	 Public Shared Function SetRange(Rng As ExcelRange, Center As Boolean, Border As Style.ExcelBorderStyle, Optional ByVal Merge As Boolean = False, Optional ByVal BackGroundColor As Color = Nothing) As ExcelRange
        If Center Then
            Rng.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
            Rng.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
        End If
        Rng.Style.Border.BorderAround(Border)
        If Merge Then Rng.Merge = Merge
        If Not BackGroundColor = Nothing Then
            Rng.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
            Rng.Style.Fill.BackgroundColor.SetColor(BackGroundColor)
        End If
        Return Rng
	 End Function
	 
End Class
